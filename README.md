# pycast

Welcome to pyCast, a set of simple Python scripts that will generate XML files for serving podcasts, and archive a podcast feed from it's RSS feed.

To generate files:

```bash
# ./pyCast.py -d ~/Public/Podcast -w http://172.31.2.250/Podcast
```

This will search all sub-directories under ~/Public/Podcast, and generate an XMl file named after each directory found.

The base URL for the files will be http://172.31.2.250/Podcast.  In this instance, the directory ~/Public was being served by Python ( sudo python -m SimpleHTTPServer 80 )

Testing has done on 10.10 / 10.11 using the pre-installed Python

NOTE: This currently only works on Python2, on a Mac, with the Py-ObjC bridge installed.


To archive a podcasts feed:

```bash
# ./archivePodcast.py -d ~/Downloads/Podcasts -u http://www.woodenovercoats.com/episodes\?format\=rss -s "Wooden Overcoats"
```

The script will "sort" the downloads into seasons, as long as the feed has the necessary tags, otherwise it'll download everything as season 0.

NOTE: This script is for Py3.