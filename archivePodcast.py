#!/usr/bin/env python
"""
"""
import os
import sys
import argparse

from json import dumps
from collections import UserDict
from urllib.parse import urlparse
from os.path import basename, join
from multiprocessing.pool import ThreadPool

from requests import get as httpGet
import feedparser



def stripnulls(data):
    "strip whitespace and nulls"
    return data.replace("\00", "").strip()

class FileInfo(UserDict):
    "store file metadata"
    def __init__(self, filename=None):
        UserDict.__init__(self)
        self["name"] = filename

class MP3FileInfo(FileInfo):
    "store ID3v1.0 MP3 tags"
    tagDataMap = {"title"   : (  3,  33, stripnulls),
                  "artist"  : ( 33,  63, stripnulls),
                  "album"   : ( 63,  93, stripnulls),
                  "year"    : ( 93,  97, stripnulls),
                  "comment" : ( 97, 126, stripnulls),
                  "genre"   : (127, 128, ord)}

    def __parse(self, filename):
        "parse ID3v1.0 tags from MP3 file"
        self.clear()
        try:                               
            fsock = open(filename, "rb", 0)
            try:                           
                fsock.seek(-128, 2)        
                tagdata = fsock.read(128)  
            finally:                       
                fsock.close()              
            if tagdata[:3] == "TAG":
                for tag, (start, end, parseFunc) in self.tagDataMap.items():
                    self[tag] = parseFunc(tagdata[start:end])               
        except IOError:                    
            pass                           

    def __setitem__(self, key, item):
        if key == "name" and item:
            self.__parse(item)
        FileInfo.__setitem__(self, key, item)


def checkFeed( feedURL: str ) -> dict:
    rssFeed = feedparser.parse( feedURL )
    for indivPost in rssFeed.entries:
        yield indivPost

def getPodcast( feedUrl: str, destDir: str, showName: str, doMultiple: bool, dryRun: bool ) -> None:
    tPool = None
    urlList = []
    if doMultiple:
        tPool = ThreadPool( 5 )
    for episodeDets in checkFeed( feedUrl ):
        if not dryRun:
            if prepareDir( destDir, showName, episodeDets[ 'itunes_season' ] if 'itunes_season' in episodeDets else '0' ): pass
            if 'links' in episodeDets:
                mediaDetails = episodeDets[ 'links' ][ 1 ]
                if 'href' in mediaDetails:
                    if doMultiple:
                        urlList.append( ( mediaDetails[ 'href' ], os.path.join( destDir, os.path.join( showName, episodeDets[ 'itunes_season' ] if 'itunes_season' in episodeDets else '0' ) ) ) )
                    else:
                        getFile( mediaDetails[ 'href' ], os.path.join( destDir, os.path.join( showName, episodeDets[ 'itunes_season' ] if 'itunes_season' in episodeDets else '0' ) ) )
        else:
            print( dumps( episodeDets ) )

    if doMultiple and not dryRun:
        dlResults = tPool.starmap( getFile, urlList )

def prepareDir( baseDir: str, feedDir: str, seasonDir: str ) -> bool:
    dirStatus = True
    fullDir = os.path.join( baseDir, os.path.join( feedDir, seasonDir ) )
    if not ( os.path.exists( fullDir ) and os.path.isdir( fullDir ) ):
        try:
            os.makedirs( fullDir )
        except OSError as errMsg:
            dirStatus = False
            print( errMsg )
    return dirStatus

def getFile( fileUrl: str, fileDst: str ) -> bool:
    dlStatus = True
    urlParts = urlparse( fileUrl )
    try:
        with httpGet( fileUrl, stream = True ) as httpResp:
            with open( os.path.join( fileDst, basename( urlParts.path ) ), 'wb' ) as localFile:
                for filePart in httpResp.iter_content( chunk_size = 8192 ):
                    if filePart:
                        localFile.write( filePart )
    except Exception as errMsg:
        print( "Encountered Error Downloading {} To {} - {}".format( fileUrl, fileDst, errMsg ) )
        dlStatus = False

    return dlStatus



def main():
    cliParser = argparse.ArgumentParser()
    cliParser.add_argument( '-d', '--dir', help = "Specifiy base directory in which to save podcasts ( podcast will be saved in own directory ).", required = True )
    cliParser.add_argument( '-u', '--url', help = "Specifiy URL of RSS feed.", required = True )
    cliParser.add_argument( '-s', '--show', help = "Specifiy Name Of Podcast.", required = True )
    cliParser.add_argument( '-m', '--multiple', help = "Enables multiple files to be downloaded at once, a max of 5, to be a nice web citizen", action = 'store_true' )
    cliParser.add_argument( '-f', '--fix', help = "Doesn't download, just shows us what it would do", action = 'store_true' )

    cliArgs = cliParser.parse_args()

    print( "Downloading Feed: ", cliArgs.url )
    print( "Saving Files To: ", os.path.expanduser( cliArgs.dir ) )
    if os.path.exists( os.path.expanduser( cliArgs.dir ) ) and os.path.isdir( os.path.expanduser( cliArgs.dir ) ):
        getPodcast( cliArgs.url, os.path.expanduser( cliArgs.dir ), cliArgs.show, cliArgs.multiple, cliArgs.fix )


if __name__ == '__main__':
    sys.exit( main() )